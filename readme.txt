﻿# Projet RVIG : Interactions Leap Motion

Package testé avec Unity 2018.2 et Leap SDK 3.2.1

1. Créer un nouveau projet 3d
2. Importer le package
3. Ouvrir la scène ExampleRVIG, étudier les paramètres de [HandController] et le contenu du dossier Leap Motion
4. Lancer l'exécution et vérifier que le tracking fonctionne et affiche un modèle de main animé
5. Créer un nouveau Game Object contenant le script FollowLeapRVIG.cs. Etudier le script et tester.
6. Modifier le script pour tenir compte de la rotation (cf [doc leap motion](https://developer-archive.leapmotion.com/documentation/csharp/devguide/Leap_Overview.html))
7. Compléter pour obtenir le degré de fermeture de la main


Pour les règles du jeu :
1. Le main gauche est pour bouger les petits blocs. Le couleur de l'objet va transmettre au bleu quand on le prend par le main gauche. Il a aussi une fonctionnalité de manipuler les chansons. Bougez à gauche ou à droite pour changer et bougez en avance pour arrêter.
2. Le main droite est pour bouger la boîte. Il a quatre directions.
3. Contrôlez le caméra avec WASD(clavier anglais) et IJKLUO pour modifier l'angle et la position de caméra.
4. Pour commencer il y a un boutton de Start et pendant le jeu il y a aussi un boutton pour reconmmencer.